followed **Clean MVVM Architecture** patten,<br />
Used **Kotlin** language is used to make app,<br />
Used **Room** for local database,<br />
Used **Retrofit** for API call,<br />
Used **RxJava** for background threding,<br />
Used **Kodein** for dependency injection,<br />
Used **LiveData** for observing data,<br />
Used **MediatorLiveData** for combining two LiveData<br />
Used **shimmer library** for showing loading shimmer effect<br />
<br />


            **Extra Features** <br />
- Used Room to store Data locally just in case if internet goes out <br />
- Cached the API data to local database<br />
- Added Swiped down Refersh to get data<br />
- Added a timer logic for getting data from API vs Database(20 minutes)<br>
- Decopuled the code using Kodein(Dependency Injection)<br />
- Added Sorting Method in the drop down(Can sort alphabetically school name and city)
- Added NetworkState Class when making service call - If failure will show an error message



          **TO DO** <br />
  - Add Smoother Animations from screen to screen <br />
  - Write TestCases <br />
  - Should use JETPACK View Binding - Removes alot of the boiler plate code
  - Should also save SAT Scores to local database as well -- only coming from Service call as of now
